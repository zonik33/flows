public class ThreadDemo {
    public static void main(String[] args) {
        System.out.println("Главный поток запущен");
        new NewThread();
        for (int i = 50; i > 10; i--) {
            System.out.println(i);
            try {
                Thread.sleep(300);
            } catch (InterruptedException e) {
                System.out.println("Главный поток прерван");
            }
        }
        System.out.println("Главный поток завершен");
    }
}