/**
 * Класс, реализующий интерфейс Runnable
 * @author Костюков К.П
 */

public class NewThread implements Runnable {
    Thread thread;

    public NewThread() {
        thread = new Thread(this, "Новый поток");
        System.out.println(thread);
        thread.start();
    }

    @Override
    public void run() {
        System.out.println("Дочерний поток запущен");
        for (int i = 30; i > 0; i--) {
            System.out.println(i);
            try {
                Thread.sleep(350);
            } catch (InterruptedException e) {
                System.out.println("Дочерний поток прерван");
            }
        }
        System.out.println("Дочерний поток завершен");
    }
}